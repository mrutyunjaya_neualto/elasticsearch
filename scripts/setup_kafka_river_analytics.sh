#!/bin/bash
HOST=''
BROKER1_HOST=$2
BROKER2_HOST=$3
BROKER3_HOST=$4
ZOOKEEPER_HOST1=$5
ZOOKEEPER_HOST2=$6
ZOOKEEPER_HOST3=$7
TOPIC_OPS="Analytics.Ops"
TOPIC_USERS="Analytics.Users"
if [ $1 ]
    then
    HOST=$1
fi

curl -XDELETE "$HOST:9200/_river/kafka_river_ops_0/_meta"
curl -XDELETE "$HOST:9200/_river/kafka_river_ops_1/_meta"
curl -XDELETE "$HOST:9200/_river/kafka_river_ops_2/_meta"
curl -XDELETE "$HOST:9200/_river/kafka_river_users_0/_meta"
curl -XDELETE "$HOST:9200/_river/kafka_river_users_1/_meta"
curl -XDELETE "$HOST:9200/_river/kafka_river_users_2/_meta"

curl -XPUT "$HOST:9200/_river/kafka_river_ops_0/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER1_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_OPS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'
curl -XPUT "$HOST:9200/_river/kafka_river_ops_1/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER2_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_OPS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'

curl -XPUT "$HOST:9200/_river/kafka_river_ops_2/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER3_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_OPS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'
curl -XPUT "$HOST:9200/_river/kafka_river_users_0/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER1_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_USERS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'
curl -XPUT "$HOST:9200/_river/kafka_river_users_1/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER2_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_USERS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'

curl -XPUT "$HOST:9200/_river/kafka_river_users_2/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "broker_host" : "'$BROKER3_HOST'",
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_USERS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'
