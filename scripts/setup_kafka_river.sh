#!/bin/bash
HOST=''
ZOOKEEPER_HOST1=$2
ZOOKEEPER_HOST2=$3
ZOOKEEPER_HOST3=$4
TOPIC_USERS="Analytics.Users"
if [ $1 ]
    then
    HOST=$1
fi

curl -XPUT "$HOST:9200/_river/kafka_river_users_0/_meta" -d '{
    "type" : "kafka",
    "kafka" : {
        "message_handler_factory_class" : "org.elasticsearch.river.kafka.JsonMessageHandlerFactory",
        "zookeeper" : "'$ZOOKEEPER_HOST1','$ZOOKEEPER_HOST2','$ZOOKEEPER_HOST3'",
        "topic" : "'$TOPIC_USERS'",
        "partition" : "0",
        "broker_port" : 9092
    },
    "index" : {
        "bulk_size_bytes" : 10000000,
        "bulk_timeout" : "1000ms"
    }
}'
