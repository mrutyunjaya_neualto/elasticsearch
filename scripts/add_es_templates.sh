#!/bin/bash
HOST=10.10.65.60
if [ $1 ]
    then
    HOST=$1
fi
echo "Updating host_statistics template on ${HOST}..."
curl -XDELETE ${HOST}:9200/_template/template_host_statistics
curl -XPUT ${HOST}:9200/_template/template_host_statistics -d '
{
    "template" : "host_statistics*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "cpu_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "memory_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" }
            }
        } ,
        "network_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "disk_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" }
            }
        }

    }
}
'
echo "\nUpdating container_statistics template..."
curl -XDELETE ${HOST}:9200/_template/template_container_statistics
curl -XPUT ${HOST}:9200/_template/template_container_statistics -d '
{
    "template" : "container_statistics*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "cpu_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "memory_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
       "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "network_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        }
    }
}
'
echo "\nUpdating container_events template..."
curl -XDELETE ${HOST}:9200/_template/template_container_events
curl -XPUT ${HOST}:9200/_template/template_container_events -d '
{
    "template" : "container_events*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "container_events" : {
            "_source" : { "enabled" : true },
	    "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "image" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"} 
            }
        }
    }
}
'
echo "\nUpdating log_events template..."
curl -XDELETE ${HOST}:9200/_template/template_log_events
curl -XPUT ${HOST}:9200/_template/template_log_events -d '
{
    "template" : "log_events*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "log_events" : {
            "_source" : { "enabled" : true },
	    "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"}
            }
        }
    }
}' 

echo "Updating config_statistics template..."
curl -XDELETE ${HOST}:9200/_template/template_config_statistics
curl -XPUT ${HOST}:9200/_template/template_config_statistics -d '
{
    "template" : "config_statistics*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "TenantStats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "tenantId" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"}
            }
        },
        "HostStats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" },
                "hostName" : { "type": "string", "index" : "not_analyzed" },
                "hostUUID" : { "type": "string", "index" : "not_analyzed" },
                "tenantId" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"}
            }
        },
        "ContainerStats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupId" : { "type": "string", "index" : "not_analyzed" },
                "hostGroupName" : { "type": "string", "index" : "not_analyzed" },
                "hostName" : { "type": "string", "index" : "not_analyzed" },
                "hostUUID" : { "type": "string", "index" : "not_analyzed" },
                "containerUID" : { "type": "string", "index" : "not_analyzed" },
                "serviceName" : { "type": "string", "index" : "not_analyzed" },
                "applicationName" : { "type": "string", "index" : "not_analyzed" },
                "tenantId" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"}
            }
        }
    }
}'

echo "Updating task_statistics template..."
curl -XDELETE  ${HOST}:9200/_template/template_task_statistics
curl -XPUT ${HOST}:9200/_template/template_task_statistics -d '
{
    "template" : "task_statistics*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "task_statistics" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "taskId" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"}
            }
        }
    }
}'
echo "Updating audit_trail template..."
curl -XDELETE  ${HOST}:9200/_template/template_audit_trail
curl -XPUT ${HOST}:9200/_template/template_audit_trail -d '
{
    "template" : "audit_trail*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "AuditTrail" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "tenantId" : { "type": "string", "index" : "not_analyzed" },
                "changeId" : { "type": "string", "index" : "not_analyzed" },
                "sequenceId" : { "type": "string", "index" : "not_analyzed" },
                "modelId" : { "type": "string", "index" : "not_analyzed" },
                "id" : { "type": "string", "index" : "not_analyzed" },
                "name" : { "type": "string", "index" : "not_analyzed" },
                "userId" : { "type": "string", "index" : "not_analyzed" },
                "userEmail" : { "type": "string", "index" : "not_analyzed" },
                "userName" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"},
                "changes" : {
                   "type" : "nested",
                   "properties": {
                       "name" : {"type": "string",  "index" : "not_analyzed" },
                       "priorValue" : {"type": "string",  "index" : "not_analyzed" },
                       "value" : {"type": "string",  "index" : "not_analyzed" }
                    }
                },
                "parentScopes" : {
                   "type" : "nested",
                   "properties": {
                       "modelIndex" : {"type": "string",  "index" : "not_analyzed" },
                       "name" : {"type": "string",  "index" : "not_analyzed" },
                       "service" : {"type": "string",  "index" : "not_analyzed" },
                       "id" : {"type": "string",  "index" : "not_analyzed" }
                    }
                }
            }
        }
    }
}'

echo "Updating billing template..."
curl -XDELETE  ${HOST}:9200/_template/template_billing
curl -XPUT ${HOST}:9200/_template/template_billing -d '
{
    "template" : "billing_record*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "BillingRecord" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "tenantId" : { "type": "string", "index" : "not_analyzed" },
                "timestamp" : { "type": "date", "index" : "not_analyzed", "format" : "date_time_no_millis"} 
            }
        }
    }
}'
