#!/bin/bash
echo 'Setting up templates...'
./add_es_templates.sh 10.10.64.60
./add_es_templates.sh 10.10.65.60
./add_es_templates.sh 10.10.66.60
echo 'Setting up kafka river...'
./setup_kafka_river.sh  10.10.65.60 10.10.64.60 10.10.65.60 10.10.66.60
echo 'Setting up marvel...'
./setup_marvel.sh 10.10.65.60 10.10.64.60 10.10.65.60 10.10.66.60
