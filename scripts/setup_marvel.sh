#!/bin/bash
HOST=''
ELASTICSEARCH_HOST1=$2
ELASTICSEARCH_HOST2=$3
ELASTICSEARCH_HOST3=$4
if [ $1 ]
    then
    HOST=$1
fi

curl -XPUT "$HOST:9200/_cluster/settings" -d '{
    "transient" : {
        "marvel.agent.exporter.es.hosts" : ["'$ELASTICSEARCH_HOST1':9200","'$ELASTICSEARCH_HOST2':9200","'$ELASTICSEARCH_HOST3':9200"]
    }
}'
