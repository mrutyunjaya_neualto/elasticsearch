#!/bin/bash
HOST=10.11.17.120
if [ $1 ]
    then
    HOST=$1
fi

echo "\nUpdating container_statistics template..."
curl -XDELETE ${HOST}:9200/_template/template_container_statistics
curl -XPUT ${HOST}:9200/_template/template_container_statistics -d '
{
    "template" : "container_statistics*",
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "cpu_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "memory_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
       "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        },
        "network_stats" : {
            "_source" : { "enabled" : true },
            "_timestamp" : {
                "enabled" : true,
                "path" : "timestamp"
            },
            "properties" : {
                "hostId" : { "type": "string", "index" : "not_analyzed" },
                "rootId" : { "type": "string", "index" : "not_analyzed" },
                "containerModelId" : { "type": "string", "index" : "not_analyzed" },
                "serviceId" : { "type": "string", "index" : "not_analyzed" },
                "desiredServiceId" : { "type": "string", "index" : "not_analyzed" },
                "serviceInstanceId" : { "type": "string", "index" : "not_analyzed" },
                "cloudProviderId" : { "type": "string", "index" : "not_analyzed" },
                "environmentId" : { "type": "string", "index" : "not_analyzed" }
            }
        }
    }
}
'
