#!/bin/bash
echo 'Setting up templates...'
ES_HOST_1=10.10.64.10
ES_HOST_2=$ES_HOST_1
ES_HOST_3=10.10.66.60
HOST_1=10.10.128.249
HOST_2=10.10.128.250
HOST_3=10.10.128.251
./add_es_templates.sh $ES_HOST_1
#./add_es_templates.sh $HOST_2
#./add_es_templates.sh $HOST_3
echo 'Setting up kafka river...'
./setup_kafka_river_analytics.sh  $ES_HOST_1 $HOST_1 $HOST_2 $HOST_3 
#./setup_kafka_river.sh  $ES_HOST_1 $HOST_1 $HOST_2 $HOST_3 $HOST_1 $HOST_2 $HOST_3
#./setup_kafka_river.sh  $HOST_2 $HOST_1 $HOST_2 $HOST_3 $HOST_2
#./setup_kafka_river.sh  $HOST_3 $HOST_1 $HOST_2 $HOST_3 $HOST_3
echo 'Setting up marvel...'
./setup_marvel.sh $ES_HOST_2 $HOST_1 $HOST_2 $HOST_3 
