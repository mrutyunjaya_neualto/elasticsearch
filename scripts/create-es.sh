#!/bin/bash
IP1=$1
IP2=$2
MASTER_NAME=$3
DATA_NAME=$4
TAG=$5
CLUSTER_NAME=nirmataanalytics
DISCOVERY_STRING="[\"$IP1[9300-9301]\",\"localhost[9300-9301]\",\"$IP2[9300-9301]\"]"
# Depends on the [(number of masterable nodes/2)+1]
MINIMUM_MASTERS=1
RECOVER_AFTER_NODES=2
EXPECTED_NODES=3
RECOVER_AFTER_TIME="5m"
HOST_IP=_non_loopback_:ipv4_
PUBLISH_IP=$(hostname -I | cut -d ' ' -f1)

sudo docker stop $MASTER_NAME
sudo docker rm $MASTER_NAME
sudo docker stop $DATA_NAME
sudo docker rm $DATA_NAME


if [ $TAG == "" ]
   then
        TAG="latest"
fi
# Remove elasticsearch image
# sudo docker rmi registry.nirmata.io/nirmata/elasticsearch:$TAG

# Start Master node
sudo docker run --name $MASTER_NAME -d -p 9200:9200 -p 9300:9300 -v /es/elasticsearch/$MASTER_NAME/data:/data -v /es/elasticsearch/$MASTER_NAME/supervisor:/var/log/supervisor \
    -e NODE_NAME=$MASTER_NAME -e CLUSTER_NAME=$CLUSTER_NAME -e IS_MASTER=true -e IS_DATA=false  -e DISCOVERY_STRING=$DISCOVERY_STRING -e ES_HEAP_SIZE=4g \
    -e MINIMUM_MASTERS=$MINIMUM_MASTERS -e RECOVER_AFTER_NODES=$RECOVER_AFTER_NODES -e  EXPECTED_NODES=$EXPECTED_NODES -e RECOVER_AFTER_TIME=$RECOVER_AFTER_TIME -e HOST_IP=$HOST_IP -e TCP_PORT=9300 -e PUBLISH_IP=$PUBLISH_IP -e HTTP_PORT=9200 registry.nirmata.io/nirmata/elasticsearch:$TAG

# Start data node
sudo docker run --name $DATA_NAME -d -p 9201:9201 -p 9301:9301 -v /es/elasticsearch/$DATA_NAME/data:/data -v /es/elasticsearch/$DATA_NAME/supervisor:/var/log/supervisor \
    -e NODE_NAME=$DATA_NAME -e CLUSTER_NAME=$CLUSTER_NAME -e IS_MASTER=false -e IS_DATA=true -e DISCOVERY_STRING=$DISCOVERY_STRING -e ES_HEAP_SIZE=4g \
    -e MINIMUM_MASTERS=$MINIMUM_MASTERS  -e RECOVER_AFTER_NODES=$RECOVER_AFTER_NODES -e  EXPECTED_NODES=$EXPECTED_NODES -e RECOVER_AFTER_TIME=$RECOVER_AFTER_TIME -e HOST_IP=$HOST_IP -e TCP_PORT=9301 -e PUBLISH_IP=$PUBLISH_IP -e HTTP_PORT=9201  registry.nirmata.io/nirmata/elasticsearch:$TAG
