#!/bin/bash

sleep 15
# /elasticsearch/bin/plugin -install mobz/elasticsearch-head
echo "Installing Kafka plugin....."
/elasticsearch/bin/plugin -remove elasticsearch-river-kafka
/elasticsearch/bin/plugin -url file:///tmp/elasticsearch-river-kafka-1.0.2-SNAPSHOT.zip -install elasticsearch-river-kafka
mkdir -p plugins/river-kafka/_site
